var eventList = []
eventList[0] = {
  title: "Rejection: friend or foe?",
  day: 19,
  month: 9,
  location: "inner",
  imgName: "testEventPic.png",
  text: "Холоне кров при слові «ні»? Думаєш, що краще не починати, ніж отримати відмову?" +
    " Розумієш, що страх відмови тебе зупиняє, але нічого не можеш з цим зробити? " +
    "☛В нас є класні новини: По-перше, ти не унікальний...",
  past:false
}
eventList[5] = {
  title: "Rejection: friend or foe?",
  day: 19,
  month: 9,
  location: "inner",
  imgName: "testEventPic.png",
  text: "Холоне кров при слові «ні»? Думаєш, що краще не починати, ніж отримати відмову?" +
    " Розумієш, що страх відмови тебе зупиняє, але нічого не можеш з цим зробити? " +
    "☛В нас є класні новини: По-перше, ти не унікальний...",
  past:false
}
eventList[2] = {
  title: "Rejection: friend or ooe?",
  day: 19,
  month: 9,
  location: "inner",
  imgName: "testEventPic.png",
  text: "Холоне кров при слові «ні»? Думаєш, що краще не починати, ніж отримати відмову?" +
    " Розумієш, що страх відмови тебе зупиняє, але нічого не можеш з цим зробити? " +
    "☛В нас є класні новини: По-перше, ти не унікальний...",
  past:false
}
eventList[3] = {
  title: "Rejection: friend or foe?",
  day: 19,
  month: 9,
  location: "inner",
  imgName: "testEventPic.png",
  text: "Холоне кров при слові «ні»? Думаєш, що краще не починати, ніж отримати відмову?" +
    " Розумієш, що страх відмови тебе зупиняє, але нічого не можеш з цим зробити? " +
    "☛В нас є класні новини: По-перше, ти не унікальний...",
  past:false
}
eventList[4] = {
  title: "Rejection: friend or joe?",
  day: 22,
  month: 12,
  location: "outer",
  imgName: "testEventPic.png",
  text: "Холоне кров при слові «ні»? Думаєш, що краще не починати, ніж отримати відмову?" +
    " Розумієш, що страх відмови тебе зупиняє, але нічого не можеш з цим зробити? " +
    "☛В нас є класні новини: По-перше, ти не унікальний...",
  past:false
}
eventList[1] = {
  title: "Rejection: friend or joe?",
  day: 22,
  month: 12,
  location: "outer",
  imgName: "testEventPic.png",
  text: "Холоне кров при слові «ні»? Думаєш, що краще не починати, ніж отримати відмову?" +
    " Розумієш, що страх відмови тебе зупиняє, але нічого не можеш з цим зробити? " +
    "☛В нас є класні новини: По-перше, ти не унікальний...",
  past:false
}
eventList[6] = {
  title: "Rejection: friend or joe?",
  day: 22,
  month: 12,
  location: "outer",
  imgName: "testEventPic.png",
  text: "Холоне кров при слові «ні»? Думаєш, що краще не починати, ніж отримати відмову?" +
    " Розумієш, що страх відмови тебе зупиняє, але нічого не можеш з цим зробити? " +
    "☛В нас є класні новини: По-перше, ти не унікальний...",
  past:false
}
eventList[7] = {
  title: "Rejection: friend or joe?",
  day: 22,
  month: 12,
  location: "outer",
  imgName: "testEventPic.png",
  text: "Холоне кров при слові «ні»? Думаєш, що краще не починати, ніж отримати відмову?" +
    " Розумієш, що страх відмови тебе зупиняє, але нічого не можеш з цим зробити? " +
    "☛В нас є класні новини: По-перше, ти не унікальний...",
  past:false
}

var pastEventList = []
pastEventList[0] = {
  title: "Rejection: friend or foe?",
  day: 19,
  month: 9,
  location: "inner",
  imgName: "testEventPic.png",
  text: "Минулого четверга разом з Yuliia Matsaienko ми говорили про відмови та що за " +
    "ними стоїть, як перетворити відмови на можливості та які стратегії ми найчастіше " +
    "використовуємо😉. Виявляється, що відмова та фізичний біль близькі ...",
  past:true
}
pastEventList[1] = {
  title: "Rejection: friend or foe?",
  day: 19,
  month: 9,
  location: "inner",
  imgName: "testEventPic.png",
  text: "Минулого четверга разом з Yuliia Matsaienko ми говорили про відмови та що за " +
    "ними стоїть, як перетворити відмови на можливості та які стратегії ми найчастіше " +
    "використовуємо😉. Виявляється, що відмова та фізичний біль близькі ...",
  past:true
}
pastEventList[2] = {
  title: "Rejection: friend or jojo?",
  day: 19,
  month: 9,
  location: "outer",
  imgName: "testEventPic.png",
  text: "Минулого четверга разом з Yuliia Matsaienko ми говорили про відмови та що за " +
    "ними стоїть, як перетворити відмови на можливості та які стратегії ми найчастіше " +
    "використовуємо😉. Виявляється, що відмова та фізичний біль близькі ...",
  past:true
}
pastEventList[3] = {
  title: "Rejection: friend or foe?",
  day: 19,
  month: 9,
  location: "inner",
  imgName: "testEventPic.png",
  text: "Минулого четверга разом з Yuliia Matsaienko ми говорили про відмови та що за " +
    "ними стоїть, як перетворити відмови на можливості та які стратегії ми найчастіше " +
    "використовуємо😉. Виявляється, що відмова та фізичний біль близькі ...",
  past:true
}
pastEventList[4] = {
  title: "Rejection: friend or foe?",
  day: 19,
  month: 9,
  location: "inner",
  imgName: "testEventPic.png",
  text: "Минулого четверга разом з Yuliia Matsaienko ми говорили про відмови та що за " +
    "ними стоїть, як перетворити відмови на можливості та які стратегії ми найчастіше " +
    "використовуємо😉. Виявляється, що відмова та фізичний біль близькі ...",
  past:true
}
pastEventList[5] = {
  title: "Rejection: friend or foe?",
  day: 19,
  month: 9,
  location: "inner",
  imgName: "testEventPic.png",
  text: "Минулого четверга разом з Yuliia Matsaienko ми говорили про відмови та що за " +
    "ними стоїть, як перетворити відмови на можливості та які стратегії ми найчастіше " +
    "використовуємо😉. Виявляється, що відмова та фізичний біль близькі ...",
  past:true
}
pastEventList[6] = {
  title: "Rejection: friend or foe?",
  day: 19,
  month: 9,
  location: "inner",
  imgName: "testEventPic.png",
  text: "Минулого четверга разом з Yuliia Matsaienko ми говорили про відмови та що за " +
    "ними стоїть, як перетворити відмови на можливості та які стратегії ми найчастіше " +
    "використовуємо😉. Виявляється, що відмова та фізичний біль близькі ...",
  past:true
}
pastEventList[7] = {
  title: "Rejection: friend or foe?",
  day: 19,
  month: 9,
  location: "inner",
  imgName: "testEventPic.png",
  text: "Минулого четверга разом з Yuliia Matsaienko ми говорили про відмови та що за " +
    "ними стоїть, як перетворити відмови на можливості та які стратегії ми найчастіше " +
    "використовуємо😉. Виявляється, що відмова та фізичний біль близькі ...",
  past:true
}
pastEventList[8] = {
  title: "Rejection: friend or foe?",
  day: 19,
  month: 9,
  location: "outer",
  imgName: "testEventPic.png",
  text: "Минулого четверга разом з Yuliia Matsaienko ми говорили про відмови та що за " +
    "ними стоїть, як перетворити відмови на можливості та які стратегії ми найчастіше " +
    "використовуємо😉. Виявляється, що відмова та фізичний біль близькі ...",
  past:true
}
pastEventList[9] = {
  title: "Rejection: friend or foe?",
  day: 19,
  month: 9,
  location: "outer",
  imgName: "testEventPic.png",
  text: "Минулого четверга разом з Yuliia Matsaienko ми говорили про відмови та що за " +
    "ними стоїть, як перетворити відмови на можливості та які стратегії ми найчастіше " +
    "використовуємо😉. Виявляється, що відмова та фізичний біль близькі ...",
  past:true
}
pastEventList[10] = {
  title: "Rejection: friend or foe?",
  day: 19,
  month: 9,
  location: "inner",
  imgName: "testEventPic.png",
  text: "Минулого четверга разом з Yuliia Matsaienko ми говорили про відмови та що за " +
    "ними стоїть, як перетворити відмови на можливості та які стратегії ми найчастіше " +
    "використовуємо😉. Виявляється, що відмова та фізичний біль близькі ...",
  past:true
}
pastEventList[11] = {
  title: "Rejection: friend or foe?",
  day: 19,
  month: 9,
  location: "inner",
  imgName: "testEventPic.png",
  text: "Минулого четверга разом з Yuliia Matsaienko ми говорили про відмови та що за " +
    "ними стоїть, як перетворити відмови на можливості та які стратегії ми найчастіше " +
    "використовуємо😉. Виявляється, що відмова та фізичний біль близькі ...",
  past:true
}
pastEventList[12] = {
  title: "Rejection: friend or foe?",
  day: 19,
  month: 9,
  location: "outer",
  imgName: "testEventPic.png",
  text: "Минулого четверга разом з Yuliia Matsaienko ми говорили про відмови та що за " +
    "ними стоїть, як перетворити відмови на можливості та які стратегії ми найчастіше " +
    "використовуємо😉. Виявляється, що відмова та фізичний біль близькі ...",
  past:true
}
pastEventList[13] = {
  title: "Rejection: friend or foe?",
  day: 19,
  month: 9,
  location: "outer",
  imgName: "testEventPic.png",
  text: "Минулого четверга разом з Yuliia Matsaienko ми говорили про відмови та що за " +
    "ними стоїть, як перетворити відмови на можливості та які стратегії ми найчастіше " +
    "використовуємо😉. Виявляється, що відмова та фізичний біль близькі ...",
  past:true
}

let plannedChangeCounter = 1;
let pastChangeCounter = 1;