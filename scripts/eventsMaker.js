function dateFormater(value) {
  if (value>10){
    return value;
  } else {
    return "0"+value;
  }
}
function registerBtnRegulator(list) {
  if (list == eventList){
    return "<div class=\"registerBtn\"></div>\n"
  } else {
    return ""
  }
}
function showEvents(id,list) {
  $("#"+id).append("<div class=\"eventTemplate__event\">\n" +
    "      <div class=\"eventTemplate__event__topBox\">\n" +
    "      <img src=\"./img/" + list[i].imgName + "\" alt=\"Event picture\">\n" +
    "      <div class=\"eventTemplate__event__date\">\n" +
    "        <div></div>\n" +
    "       <p>" + dateFormater(list[i].day) + ".<span>" + dateFormater(list[i].month) + ".</span></p>\n" +
    "      </div>\n" +
    "      <div class=\"eventTemplate__event__" + list[i].location + "\"></div>\n" +
    "      </div>\n" +
          registerBtnRegulator(list) +
    "      <p class=\"eventTemplate__event__title\">" + list[i].title + "</p>\n" +
    "      <p class=\"eventTemplate__event__text\">" + list[i].text + " </p>\n" +
    "    </div>")
  $('.eventTemplate__event__outer').html('Зовнішній')
  $('.eventTemplate__event__inner').html('Внутрішній')
  $('.registerBtn').html('РЕЄСТРАЦІЯ');
}
function clearEvents(id) {
  $('#'+id).empty();
}


function showThree1() {
  for (i=0;i<3;i++){
    showEvents("plannedEventsContent",eventList);
  }
}
function showAll1() {
  for (i=0;i<eventList.length;i++){
    showEvents("plannedEventsContent",eventList);
  }
}

function showThreeOuters1(){
  for (i=0,j=0;i<eventList.length,j<3;i++){
    if (eventList[i].location=="outer"){
      showEvents("plannedEventsContent",eventList);
      j++;
    } else {
      continue
    }
  }
}
function showAllOuters1(){
  for (i=0;i<eventList.length;i++){
    if (eventList[i].location=="outer"){
      showEvents("plannedEventsContent",eventList);
    } else {
      continue
    }
  }
}

function showThreeInners1(){
  for (i=0,j=0;i<eventList.length,j<3;i++){
    if (eventList[i].location=="inner"){
      showEvents("plannedEventsContent",eventList);
      j++
    } else {
      continue
    }
  }
}
function showAllInners1(){
  for (i=0;i<eventList.length;i++){
    if (eventList[i].location=="inner"){
      showEvents("plannedEventsContent",eventList);
    } else {
      continue
    }
  }
}


function showThree2() {
  for (i=0;i<3;i++){
    showEvents("pastEventsContent",pastEventList);
  }
}
function showAll2() {
  for (i=0;i<pastEventList.length;i++){
    showEvents("pastEventsContent",pastEventList);
  }
}

function showThreeOuters2(){
  for (i=0,j=0;i<pastEventList.length,j<3;i++){
    if (pastEventList[i].location == "outer"){
      showEvents("pastEventsContent",pastEventList);
      j++;
    } else {
      continue
    }
  }
}
function showAllOuters2(){
  for (i=0;i<pastEventList.length;i++){
    if (pastEventList[i].location == "outer"){
      showEvents("pastEventsContent",pastEventList);
    } else {
      continue
    }
  }
}

function showThreeInners2(){
  for (i=0,j=0;i<pastEventList.length,j<3;i++){
    if (pastEventList[i].location=="inner"){
      showEvents("pastEventsContent",pastEventList);
      j++;
    } else {
      continue
    }
  }
}
function showAllInners2(){
  for (i=0;i<pastEventList.length;i++){
    if (pastEventList[i].location=="inner"){
      showEvents("pastEventsContent",pastEventList);
    } else {
      continue
    }
  }
}

function eventOn(id) {
  if (id=="#allEvents1"){
    $("#allEvents1").removeClass("event-on").addClass("event-on")
    $("#outerEvents1").removeClass("event-on")
    $("#innerEvents1").removeClass("event-on")
  } else if (id == "#outerEvents1"){
    $("#outerEvents1").removeClass("event-on").addClass("event-on")
    $("#allEvents1").removeClass("event-on")
    $("#innerEvents1").removeClass("event-on")
  } else if (id == "#innerEvents1"){
    $("#innerEvents1").removeClass("event-on").addClass("event-on")
    $("#allEvents1").removeClass("event-on")
    $("#outerEvents1").removeClass("event-on")
  } else if (id=="#allEvents2"){
    $("#allEvents2").removeClass("event-on").addClass("event-on")
    $("#outerEvents2").removeClass("event-on")
    $("#innerEvents2").removeClass("event-on")
  } else if (id == "#outerEvents2"){
    $("#outerEvents2").removeClass("event-on").addClass("event-on")
    $("#allEvents2").removeClass("event-on")
    $("#innerEvents2").removeClass("event-on")
  } else if (id == "#innerEvents2") {
    $("#innerEvents2").removeClass("event-on").addClass("event-on")
    $("#allEvents2").removeClass("event-on")
    $("#outerEvents2").removeClass("event-on")
  }
}
function showStarter(page){
  if (page == "main"){
      if (plannedChangeCounter == 1){
        showThree1()
      } else if (plannedChangeCounter == 2){
        showThreeOuters1()
      } else if (plannedChangeCounter == 3){
        showThreeInners1()
      }
      if (pastChangeCounter == 1){
        showThree2()
      } else if (pastChangeCounter == 2){
        showThreeOuters2()
      } else if (pastChangeCounter == 3){
        showThreeInners2()
      }
  }
  else if (page == "past"){
    $("#pastEventsTitle").addClass("event-on");
      if (pastChangeCounter == 1){
        showAll2()
      } else if (pastChangeCounter == 2){
       showAllOuters2()
     } else if (pastChangeCounter == 3){
       showAllInners2()
  }
  }
  else if (page == "future"){
    $("#plannedEventsTitle").addClass("event-on");
      if (plannedChangeCounter == 1){
        showAll1()
      } else if (plannedChangeCounter == 2){
        showAllOuters1()
      } else if (plannedChangeCounter == 3){
        showAllInners1()
      }
    }

}

$('.eventTemplate__menu__allEvents').css('cursor', 'pointer');
$('.eventTemplate__menu__outerEvents').css('cursor', 'pointer');
$('.eventTemplate__menu__innerEvents').css('cursor', 'pointer');
$('.eventTemplate__event__outer').html('Зовнішній');
$('.eventTemplate__event__inner').html('Внутрішній');